package com.nixsolutions.SimpleService.model;

import java.util.ArrayList;
import java.util.List;

import com.nixsolutions.SimpleService.entity.StudentGroup;

public class Data {

	private static List<StudentGroup> data;
	private static long count = 5;
	
	static {
		data = new ArrayList<StudentGroup>();
		data.add(new StudentGroup(1L, "First"));
		data.add(new StudentGroup(2L, "Second"));
		data.add(new StudentGroup(3L, "Third"));
		data.add(new StudentGroup(4L, "Fourth"));
	}
	
	public static List<StudentGroup> getData() {
		return data;
	}

	public static StudentGroup findStudentGroupById(long id) {
		for (StudentGroup studentGroup : data) {
			if (studentGroup.getStudentGroupId() == id) {
				return studentGroup;
			}
		}
		return null;
	}

	public static boolean deleteStudentGroupById(long id) {
		boolean result = false;
		for (StudentGroup studentGroup : data) {
			if (studentGroup.getStudentGroupId() == id) {
				result = data.remove(studentGroup);
				return result;
			}
		}
		return result;
	}

	public static boolean updateStudentGroup(StudentGroup studentGroup) {
		boolean result = false;
		for (StudentGroup temp: data) {
			if (temp.getStudentGroupId() == studentGroup.getStudentGroupId()) {
				temp.setStudentGroupName(studentGroup.getStudentGroupName());
				result = true;
			}
		}
		return result;
	}

	public static Long addStudentGroup(StudentGroup studentGroup) {
		studentGroup.setStudentGroupId(count);
		count++;
		boolean result = data.add(studentGroup);
		if (result)
			return count;
		else
			return -1L;
	}
}
