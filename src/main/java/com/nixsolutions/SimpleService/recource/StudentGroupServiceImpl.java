package com.nixsolutions.SimpleService.recource;

import java.util.List;

import com.nixsolutions.SimpleService.entity.StudentGroup;
import com.nixsolutions.SimpleService.model.Data;

public class StudentGroupServiceImpl implements StudentGroupService {

	@Override
	public List<StudentGroup> getAll() {
		List<StudentGroup> list = Data.getData();
		if (list == null) {
			throw new RuntimeException("Can't load student group list");
		}
		return list;
	}

	@Override
	public StudentGroup create(StudentGroup studentGroup) {
		long index = Data.addStudentGroup(studentGroup);
		return Data.findStudentGroupById(index);
	}

	@Override
	public StudentGroup getById(int id) {
		StudentGroup studentGroup = Data.findStudentGroupById(id);
		if (studentGroup == null) {
			throw new RuntimeException("can't find student group with id = " + id);
		}
		return studentGroup;
	}

	@Override
	public StudentGroup update(StudentGroup studentGroup) {
		long id = studentGroup.getStudentGroupId();
		if(!Data.updateStudentGroup(studentGroup)){
			throw new RuntimeException("can't update student group with id = " + id);
		}
		return Data.findStudentGroupById(id);
	}

	@Override
	public void delete(long id) {
		if(Data.deleteStudentGroupById(id)){
			throw new RuntimeException("can't delete student group with id = " + id);
		}
	}

}
