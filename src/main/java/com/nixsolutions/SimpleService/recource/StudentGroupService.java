package com.nixsolutions.SimpleService.recource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.nixsolutions.SimpleService.entity.StudentGroup;

@Path("/")
public interface StudentGroupService {

	@GET
	@Path("/")
	@Produces("application/xml")
	public List<StudentGroup> getAll();
	
	@POST
	@Path("/")
	@Consumes({"application/xml"})
	@Produces("application/xml")
	public StudentGroup create(StudentGroup studentGroup);

	@GET
	@Path("/{id}")
	@Produces("application/xml")
	public StudentGroup getById(@PathParam("id")int id);
	
	@PUT
	@Path("/{id}")
	@Consumes({"application/xml"})
	@Produces("application/xml")
	public StudentGroup update(StudentGroup studentGroup);
	
	@DELETE
	@Path("/{id}")
	public void delete(@PathParam("id") long id);	
	
}
